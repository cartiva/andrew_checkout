-- techs ---------------------------------------------------------------
SELECT *
-- SELECT COUNT(*) -- 33
FROM dimtech a
--inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber = true
WHERE a.active = true
  AND a.flagdeptcode = 'mr'
  AND a.currentrow = true
  AND a.employeenumber <> 'NA'
  AND a.storecode = 'RY1'
ORDER BY name  


SELECT b.employeekey
-- SELECT a.*
-- SELECT COUNT(*) -- 29
FROM dimtech a
inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.active = 'active'
  AND b.currentrow = true
WHERE a.active = true
  AND a.flagdeptcode = 'mr'
  AND a.currentrow = true
  AND a.employeenumber <> 'NA'
  AND a.storecode = 'RY1'
  AND a.employeenumber <> '1117960' -- exclude craig rogne

-- dates ----------------------------------------------------------------------
select datekey, thedate, dayofweek, LEFT(dayname, 3), mmdd
-- SELECT *
FROM day 
WHERE sundaytosaturdayweek = (
  SELECT sundaytosaturdayweek
  FROM day
  WHERE thedate = curdate())
  
SELECT *
FROM factClockHoursToday a
INNER JOIN (
  SELECT b.employeekey
  FROM dimtech a
  inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.active = 'active'
    AND b.currentrow = true
  WHERE a.active = true
    AND a.flagdeptcode = 'mr'
    AND a.currentrow = true
    AND a.employeenumber <> 'NA'
    AND a.storecode = 'RY1'
    AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
INNER JOIN (
  select datekey, thedate, dayofweek, LEFT(dayname, 3), mmdd
  FROM day 
  WHERE sundaytosaturdayweek = (
    SELECT sundaytosaturdayweek
    FROM day
    WHERE thedate = curdate())) bb on a.datekey = bb.datekey
      AND bb.dayofweek BETWEEN 2 AND 6   
  

-- ok, time out, i am thinking of designing the whole fucking spreadsheet
-- just talked to andrew, i just need to send him the numbers
-- twice a day, 5PM AND 5AM

-- clockhours today
SELECT -- clockhours today
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  2, 'Tech Clock Hours', SUM(clockhours) AS clockhours  --205.92
FROM factClockHoursToday a
INNER JOIN (
  SELECT b.employeekey
  FROM dimtech a
  inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.active = 'active'
    AND b.currentrow = true
  WHERE a.active = true
    AND a.flagdeptcode = 'mr'
    AND a.currentrow = true
    AND a.employeenumber <> 'NA'
    AND a.storecode = 'RY1'
    AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
INNER JOIN day bb on a.datekey = bb.datekey
  AND bb.thedate = curdate()
  
-- clockhours yesterday
SELECT SUM(clockhours) AS clockhours  --245.5
FROM edwClockHoursFact a
INNER JOIN (
  SELECT b.employeekey
  FROM dimtech a
  inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.active = 'active'
    AND b.currentrow = true
  WHERE a.active = true
    AND a.flagdeptcode = 'mr'
    AND a.currentrow = true
    AND a.employeenumber <> 'NA'
    AND a.storecode = 'RY1'
    AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
INNER JOIN day bb on a.datekey = bb.datekey
  AND bb.thedate = curdate() -1
  
  
  