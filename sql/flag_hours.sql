ros to include:
  CLOSE DATE
  Service Types: Aftermarket, Commercial Mech Repair, Employee, Menu Priced Service, 
                 Mechanical Repair, Specialty Vehicle/Heavy Equip
GROUP BY payment type                 

-- today 50.2/12.2/23.6
SELECT trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
FROM todayfactrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.datetype = 'DATE'
  AND b.thedate = curdate()
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
  AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
GROUP by d.paymenttype


-- yesterday  94.5/10.1/12.3/40.2
SELECT trim (d.paymenttype) + ' flag hours', round(SUM(flaghours), 1)
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.thedate = curdate() - 1
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
  AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
GROUP by d.paymenttype

