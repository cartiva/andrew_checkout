-- clockhours today
SELECT -- clockhours today
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  2, 'Tech Clock Hours', SUM(clockhours) AS clockhours  --205.92
FROM factClockHoursToday a
INNER JOIN (
  SELECT b.employeekey
  FROM dimtech a
  inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.active = 'active'
    AND b.currentrow = true
  WHERE a.active = true
    AND a.flagdeptcode = 'mr'
    AND a.currentrow = true
    AND a.employeenumber <> 'NA'
    AND a.storecode = 'RY1'
    AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
INNER JOIN day bb on a.datekey = bb.datekey
  AND bb.thedate = curdate()
UNION 
SELECT -- flaghours today  
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  CASE d.paymenttype
    WHEN 'customer pay' THEN 3
	WHEN 'internal' THEN 4
	WHEN 'warranty' THEN 5
	WHEN 'service contract' THEN 6
  END AS seq,
  trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
FROM todayfactrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.datetype = 'DATE'
  AND b.thedate = curdate()
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
  AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
GROUP by d.paymenttype  



-- email content
SELECT metric, value
FROM andrew_checkout
WHERE the_date = curdate()
  AND hour(the_time) < 12
ORDER BY seq



-- yesterday
SELECT -- clockhours yesterday
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  2, 'Tech Clock Hours', SUM(clockhours) AS clockhours 
FROM edwClockHoursFact a
INNER JOIN (
  SELECT b.employeekey
  FROM dimtech a
  inner JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.active = 'active'
    AND b.currentrow = true
  WHERE a.active = true
    AND a.flagdeptcode = 'mr'
    AND a.currentrow = true
    AND a.employeenumber <> 'NA'
    AND a.storecode = 'RY1'
    AND a.employeenumber <> '1117960') aa on a.employeekey = aa.employeekey
INNER JOIN day bb on a.datekey = bb.datekey
  AND bb.thedate = curdate() - 1
UNION 
SELECT -- flaghours yesterday
  (SELECT curdate() FROM system.iota), 
  (SELECT curtime() FROM system.iota),
  CASE d.paymenttype
    WHEN 'customer pay' THEN 3
	WHEN 'internal' THEN 4
	WHEN 'warranty' THEN 5
	WHEN 'service contract' THEN 6
  END AS seq,
  trim (d.paymenttype) + ' Flag Hours', round(SUM(flaghours), 1)
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.datetype = 'DATE'
  AND b.thedate = curdate() -1
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
  AND c.servicetypecode IN ('AM','CM','EM','MN','MR','SV')
INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
GROUP by d.paymenttype  